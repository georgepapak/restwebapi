FROM openjdk:8-jdk-alpine
VOLUME C:/temp
EXPOSE 8080
ADD target/rest-web-api-0.0.1-SNAPSHOT.jar rest-web-api.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=container", "-jar", "rest-web-api.jar"]