
Build a Docker Image with Maven

mvnw spring-boot:build-image -Dmaven.test.skip=true  -Dspring-boot.build-image.imageName=springio/gs-spring-boot-docker

1. git clone https://bitbucket.org/georgepapak/restwebapi.git

2.cd restwebapi

3.mvn clean install -DskipTests

4.docker create -v C:\temp\data --name PostgresData alpine

//docker run -p 5432:5432 --name postgres -e POSTGRES_PASSWORD=1234 -d --volumes-from PostgresData postgres

5.docker build ./ -t rest-web-api


6.docker-compose up

7.
http://localhost:8080/app/matches
http://localhost:8080/app/matches/2
http://localhost:8080/app/matchOdds
http://localhost:8080/app/matchOdds/2