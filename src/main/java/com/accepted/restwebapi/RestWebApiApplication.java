package com.accepted.restwebapi;

import com.accepted.restwebapi.model.Match;
import com.accepted.restwebapi.model.MatchOdd;
import com.accepted.restwebapi.model.SportType;
import com.accepted.restwebapi.repository.MatchOddsRepository;
import com.accepted.restwebapi.repository.MatchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class RestWebApiApplication {
	Logger logger = LoggerFactory.getLogger(RestWebApiApplication.class);

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private MatchOddsRepository matchOddsRepository;

	public static void main(String[] args) {
		SpringApplication.run(RestWebApiApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void runAfterStartup() {
		List allMatches = this.matchRepository.findAll();

		logger.info("Number of customers: " + allMatches.size());

		//1

		Match newMatch = new Match();
		newMatch.setDescription("OSFP-PAO");
		newMatch.setMatchDate(new Date());
		newMatch.setMatchTime(new Date());
		newMatch.setTeamA("OSFP");
		newMatch.setTeamB("PAO");
		newMatch.setSport(SportType.FOOTBALL);

		MatchOdd matchOdd = new MatchOdd();
		matchOdd.setSpecifier("X");
		matchOdd.setOdd(1.5);

		matchOdd.setMatch(newMatch);

		newMatch.setMatchOdds(Collections.singletonList(matchOdd));

		logger.info("Saving new customer...");
		this.matchRepository.save(newMatch);

		allMatches = this.matchRepository.findAll();
		logger.info("Number of customers: " + allMatches.size());

		//2

		newMatch = new Match();
		newMatch.setDescription("OSFP-PAOK");
		newMatch.setMatchDate(new Date());
		newMatch.setMatchTime(new Date());
		newMatch.setTeamA("OSFP");
		newMatch.setTeamB("PAOK");
		newMatch.setSport(SportType.FOOTBALL);

		matchOdd = new MatchOdd();
		matchOdd.setSpecifier("1");
		matchOdd.setOdd(0.5);

		matchOdd.setMatch(newMatch);

		newMatch.setMatchOdds(Collections.singletonList(matchOdd));

		logger.info("Saving new customer...");
		this.matchRepository.save(newMatch);

		allMatches = this.matchRepository.findAll();
		logger.info("Number of customers: " + allMatches.size());

		//3
		newMatch = new Match();
		newMatch.setDescription("OSFP-AEL");
		newMatch.setMatchDate(new Date());
		newMatch.setMatchTime(new Date());
		newMatch.setTeamA("OSFP");
		newMatch.setTeamB("AEL");
		newMatch.setSport(SportType.FOOTBALL);

		matchOdd = new MatchOdd();
		matchOdd.setSpecifier("2");
		matchOdd.setOdd(2.5);

		matchOdd.setMatch(newMatch);

		newMatch.setMatchOdds(Collections.singletonList(matchOdd));

		logger.info("Saving new match...");
		this.matchRepository.save(newMatch);

		allMatches = this.matchRepository.findAll();
		logger.info("Number of customers: " + allMatches.size());
	}

}
