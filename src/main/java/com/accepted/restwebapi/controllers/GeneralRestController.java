package com.accepted.restwebapi.controllers;

import com.accepted.restwebapi.exceptions.MatchNotFoundException;
import com.accepted.restwebapi.exceptions.MatchOddNotFoundException;
import com.accepted.restwebapi.model.Match;
import com.accepted.restwebapi.model.MatchOdd;
import com.accepted.restwebapi.repository.MatchOddsRepository;
import com.accepted.restwebapi.repository.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class GeneralRestController {


    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private MatchOddsRepository matchOddsRepository;


    @RequestMapping("/")
    public String index() {
        return "Greetings from George Papakostas!";
    }


    // Match Rest Service Calls

    @GetMapping("/matches")
    public List<Match> all() {
        return matchRepository.findAll();
    }


    @PostMapping("/matches")
    public Match newMatch(@RequestBody Match newMatch) {
        return matchRepository.save(newMatch);
    }

    // Single item

    @GetMapping("/matches/{id}")
    public Match one(@PathVariable Long id) {

        return matchRepository.findById(id)
                .orElseThrow(() -> new MatchNotFoundException(id));
    }

    @PutMapping("/matches/{id}")
    public Match replaceMatch(@RequestBody Match newMatch, @PathVariable Long id) {

        return matchRepository.findById(id)
                .map(match -> {
                    match.setDescription(newMatch.getDescription());
                    match.setMatchDate(newMatch.getMatchDate());
                    match.setMatchTime(newMatch.getMatchTime());
                    match.setTeamA(newMatch.getTeamA());
                    match.setTeamB(newMatch.getTeamB());
                    match.setMatchOdds(matchOddsRepository.findByMatchId(newMatch.getId()));
                    return matchRepository.save(match);
                })
                .orElseGet(() -> {
                    newMatch.setId(id);
                    return matchRepository.save(newMatch);
                });
    }

    @DeleteMapping("/matches/{id}")
    public void deleteMatch(@PathVariable Long id) {
        matchRepository.deleteById(id);
    }



    // MatchOdds Rest Service Calls


    @GetMapping("/matchOdds")
    public List<MatchOdd> allOdds() {
        return matchOddsRepository.findAll();
    }


    @PostMapping("/matchOdds")
    public MatchOdd newMatchOdd(@RequestBody MatchOdd newMatchOdds) {
        return matchOddsRepository.save(newMatchOdds);
    }

    // Single item

    @GetMapping("/matchOdds/{id}")
    public MatchOdd oneOdd(@PathVariable Long id) {

        return matchOddsRepository.findById(id)
                .orElseThrow(() -> new MatchOddNotFoundException(id));
    }

    @PutMapping("/matchOdds/{id}")
    public MatchOdd replaceMatchOdd(@RequestBody MatchOdd newMatchOdd, @PathVariable Long id) {

        return matchOddsRepository.findById(id)
                .map(matchOdd -> {
                    matchOdd.setOdd(newMatchOdd.getOdd());
                    matchOdd.setSpecifier(newMatchOdd.getSpecifier());
                    Optional<Match> match =  matchRepository.findById(newMatchOdd.getMatch().getId());
                    matchOdd.setMatch(match.orElse(null));
                    return matchOddsRepository.save(matchOdd);
                })
                .orElseGet(() -> {
                    newMatchOdd.setId(id);
                    return matchOddsRepository.save(newMatchOdd);
                });
    }

    @DeleteMapping("/matchOdds/{id}")
    public void deleteMatchOdds(@PathVariable Long id) {
        matchOddsRepository.deleteById(id);
    }
}