package com.accepted.restwebapi.exceptions;

public class MatchNotFoundException extends RuntimeException {

    public MatchNotFoundException(Long id) {
        super("Could not find Match " + id);
    }

}
