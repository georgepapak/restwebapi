package com.accepted.restwebapi.exceptions;

public class MatchOddNotFoundException extends RuntimeException {

    public MatchOddNotFoundException(Long id) {
        super("Could not find Match Odd " + id);
    }

}
