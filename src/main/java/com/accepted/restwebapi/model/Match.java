package com.accepted.restwebapi.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "match")
public class Match extends Identifiable implements Serializable {

    @Column(name = "description")
    private String description;

    @Column(name = "match_date")
    private Date matchDate;

    @Column(name = "match_time")
    private Date matchTime;

    @Column(name = "team_a")
    private String teamA;

    @Column(name = "team_b")
    private String teamB;

    @Column(name = "sport")
    @Enumerated(EnumType.STRING)
    private SportType sport;

    @OneToMany(mappedBy="match", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    List<MatchOdd> matchOdds;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Date match_date) {
        this.matchDate = match_date;
    }

    public Date getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(Date match_time) {
        this.matchTime = match_time;
    }

    public String getTeamA() {
        return teamA;
    }

    public void setTeamA(String team_a) {
        this.teamA = team_a;
    }

    public String getTeamB() {
        return teamB;
    }

    public void setTeamB(String team_b) {
        this.teamB = team_b;
    }

    public SportType getSport() {
        return sport;
    }

    public void setSport(SportType sport) {
        this.sport = sport;
    }

    public List<MatchOdd> getMatchOdds() {
        return matchOdds;
    }

    public void setMatchOdds(List<MatchOdd> matchOdds) {
        this.matchOdds = matchOdds;
    }
}
