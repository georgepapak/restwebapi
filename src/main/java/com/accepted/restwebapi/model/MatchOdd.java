package com.accepted.restwebapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "match_odd")
public class MatchOdd extends Identifiable implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="match_id")
    @JsonBackReference
    private Match match;

    @Column(name = "specifier")
    private String specifier;

    @Column(name = "odd")
    private Double odd;

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public String getSpecifier() {
        return specifier;
    }

    public void setSpecifier(String specifier) {
        this.specifier = specifier;
    }

    public Double getOdd() {
        return odd;
    }

    public void setOdd(Double odd) {
        this.odd = odd;
    }
}
