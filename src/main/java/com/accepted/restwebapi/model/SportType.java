package com.accepted.restwebapi.model;

public enum SportType {
    FOOTBALL,
    BASKETBALL;
}
