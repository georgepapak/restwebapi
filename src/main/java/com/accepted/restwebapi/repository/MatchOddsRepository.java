package com.accepted.restwebapi.repository;


import com.accepted.restwebapi.model.MatchOdd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchOddsRepository extends JpaRepository<MatchOdd, Long> {

    List<MatchOdd> findByMatchId(Long id);
}
